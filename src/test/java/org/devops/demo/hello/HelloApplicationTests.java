package org.devops.demo.hello;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class HelloApplicationTests {

	@Autowired
	private MockMvc mvc;

	@Test
	public void contextLoads() {
	}

	@Test
	public void testHello() throws Exception {
		this.mvc.perform(get("/hello")).andExpect(status().isOk())
				.andExpect(content().string("Hello"));
	}

	@Test
	public void testSatHello() throws Exception {
		this.mvc.perform(get("/hello/1")).andExpect(status().isOk())
				.andExpect(content().string("Hello 1"));

		this.mvc.perform(get("/hello/2")).andExpect(status().isOk())
				.andExpect(content().string("Hello 2"));		
	}

}

