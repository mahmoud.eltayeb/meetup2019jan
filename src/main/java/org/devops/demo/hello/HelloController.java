package org.devops.demo.hello;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@GetMapping("/hello/{id}")
	public String sayHello(@PathVariable String id) {
		return String.format("Hello %s", id);
	}

	@GetMapping("/hello")
	public String hello() {
		return "Hello";
	}
}
